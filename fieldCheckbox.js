forms.addGenerator('checkbox', {
 	generateField: function(params) {	
		return Template['fieldCheckbox'](params);
	},
	getValue: function(element) {
		//v("Checked:"+element.attr('checked'));
		return "checked" == element.attr('checked');		
	}
});


Template.fieldCheckbox.marked = function() {
	d("Checkbox context:",this);
	return this.value ? "checked" : "";
}

