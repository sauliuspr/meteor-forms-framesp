/*******************************
 * Class For Datepicker
 * based on https://github.com/eternicode/bootstrap-datepicker
 */
 
 Template.fieldDatepicker.rendered = function() {
	var now = new Date();
	$('#'+this.data.field)
		.datepicker({
			autoclose: true,
			todayBtn: true
		})
		.datepicker('setValue', now);
 }
 /*
 forms.addGenerator({
 	generateField: function(form, obj, propName) {
		if(form[propName].type == "datepicker" ) {
			result += 
				'<input type="text"' 
					+'class="datepicker"'
					+'value="'+(obj[propName] || "")
					+'" id="'+propName+'" >';	
			return true;
		} else {
			return false;
		}
	}
});
		*/


