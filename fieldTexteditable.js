forms.addGenerator("texteditable", {
 	generateField: function(params) {	
		return Template['fieldTexteditable'](params);
	},
	getValue: function(element) {
		return $(element).text();		
	}
	
});
