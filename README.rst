Form frame
==========

Intro
===== 
This is meteor form framework ready to used out-of-shelf and easily customizible.

Installation
============ 

	git submodule add https://spastai@bitbucket.org/spastai/meteor-forms-frame.git client/packages/forms-frame

and time after time

	git pull
	git merge

and if for some reasons you edit files in submodule, but want to update from the master
 
	cd client/packages/forms-frame
	git stash

this will discard your changes 

How to use
==========

Define your form model
	var testForm = {
			title: {type: String, label: "Issue title", placeholder: "Issue title..."},
			type: {type: "select", label: "Type", options: types, rendered: onTypeChange},
			testcase: {type: "textarea", label: "Test Case", placeholder: "Enter...", hide: checkType},
			version: {type: "listedit", label: "Version", placeholder: "Enter..."},
	};

And CRUD methods 

	forms.model("testForm", testForm, {
		create: function(template) {
			console.log("Saving test form");
			var values = forms.getValues("testForm",template);
			console.dir(values);
		}
	});

Put this in your html

	{{#with formParams "testForm" "testObj"}}
		{{> form}}
	{{/with}}

where testObj is session object name where edit values will be stored 

Concept
=======

The idea of creating one more meteor forms module is to provide very open solution - for the customizations and extensions
Thus this is more the concept, but working one. You can define your one field rendering, validation or CRUD methods, but the most
important - don't repeat yourself. This is the heart of this framework. 

Customizations
==============

Changing form
-------------

You can define your form 

	{{#with formParams "testForm" "testObj"}}
		{{> form}}
	{{/with}}


Define your fields
------------------
 

Validation
----------
Adding property 
	validation: {...}
in form description and  
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
in main html allows to use 
http://docs.jquery.com/Plugins/Validation#List_of_built-in_Validation_methods 

What's new
==========

0.0.1 
[+] Description of the project added 