/*******************************
 * Meteor
 * main.js will be loaded last 
 */
 if (Meteor.isClient) {
	/*******************************
	 * Helpers 
	 */
	Handlebars.registerHelper('formParams',function(formName, obj, fieldControll){
		var form = forms.getForm(formName);
		return getFormObject(formName, form, obj, undefined, fieldControll);
	});
	 
	Handlebars.registerHelper('formParamsGroup', function(formName, obj, group, fieldControll) {
	    group = group && group.hash ? undefined : group; 
		var form = forms.getForm(formName);
		return getFormObject(formName, form, obj, group, fieldControll);
	});
	
	Handlebars.registerHelper('bakedFormParams',function(formName, obj, fieldControll){
	    var obj = Session.get(obj) || {};
	    var collection = forms.getCollection(formName);
		form = {};		
	    if(collection) {
//	    	form = collection.findOne(id).model; 
	    }	    
	    var form = Forms.findOne({formName:formName});
	    form = form ? form.model : {};
	    
		return getFormObject(formName, form, obj, undefined, fieldControll);
	});
	
	function getFormObject(formName, form, obj, group, fieldControll) {
		var fields = [];
	    fieldControll = fieldControll && fieldControll.hash ? undefined : fieldControll; 
	    var obj = Session.get(obj) || {};
	    //d("Got new value",obj);
	    //v("Generating form with group:"+group); 
		for(p in form) {
			if(group && form[p].group && group != form[p].group) {
				//v("Breaking for group:"+group);
				continue;
			}
			// allows 'set' attributes
			if(group && form[p].set && group != form[p].set) {
				//v("Breaking for group:"+group);
				continue;
			}
			fields.push({
				field: p, 
				definition: form[p], 
				value: obj[p],
				// redundand - can't access parent data in each
				fieldControll: fieldControll
			});
		}
		//d("Fields:",fields); 
		return {
			formName:formName, 
			form:form, 
			object: obj, 
			fields: fields, 
		};
	} 

	Handlebars.registerHelper('fieldHtml',function(field, fieldControll){
		var fieldData = field.hash ? this : {field: field, definition: this.form[field]};
		fieldData.fieldControll = (fieldControll && fieldControll.hash) ? "fieldControllBare" : fieldControll;
		
		if(!fieldData.definition) {
			//d("Field not defined:",field);
			fieldData.definition = {type: "string"};
		}
		
		var type = fieldData.definition.type;
		var generate = forms.getGenerators()[type];
		//d("Field html:"+generate, this);
		var controll = generate ? generate.generateField(fieldData) : defaultGenerateField(type, fieldData);
		var group = {
			controll:controll,
			label:fieldData.definition.label
		};
		return Template[fieldData.fieldControll || 'fieldControllGroup'](group);
	});
	/* 
	Template.formFields.fieldHtml = function() {
		var type = this.definition.type;
		var generate = forms.getGenerators()[type];
		//d("Field html:"+generate, this);
		var controll = generate ? generate.generateField(this) : defaultGenerateField(type, this);
		var group = {
			controll:controll,
			label:this.definition.label
		};
		return Template[this.fieldControll || 'fieldControllGroup'](group);
	}
	*/
	
	function defaultGenerateField(type, params) {
		var template = 'field'+type;
		if(!Template[template]) { 
			template = 'field'+type.charAt(0).toUpperCase() + type.substring(1).toLowerCase();
		}
		return Template[template](params);
	}	
	
	
	
	/* 
	 * Validation is imeplemted using
	 * http://docs.jquery.com/Plugins/Validation/rules
	 *  
	 */ 
	Template.formFields.rendered = function() {
		var form = forms.getForm(this.data.name);
	
		//v("add validation");
		var rules = {};
		var hasRules = false;
		for (var propName in form) {
			if(form[propName].validation) {
				rules[propName] = form[propName].validation;
				hasRules = true;
			}
			if(form[propName].rendered) {
				form[propName].rendered(propName);
			}
		}
		
		if(hasRules) {		
			$("#"+this.data.name).validate({
				highlight: function(element, errorClass) {
			        $(element.parentNode.parentNode).addClass("error");
			    },
			    unhighlight: function(element, errorClass) {
			        $(element.parentNode.parentNode).removeClass("error");
			    },
			    rules: rules
			});
		};
		
		/*
		$('.htmledit').each(function(index) {
			CKEDITOR.replace($(this).attr('id'));
		});
		*/
	};
	
	
	// TODO .save should be changed to universal click and parameter used to crud[parameter]
	Template.form.events({
		'click .save': function (event, template) {
			var crud = forms.getCrud(template.data.formName);
			if(crud && crud.create) {
				crud.create(template);
			} else if(crud && crud.save) {
				var values = forms.getValues(template.data.formName,template);
				//d("Form values:",values);
				crud.save(values);
			}
		}
	});


	// deprecated
	Template.formFieldsDeprecated.content = function() {
		return generateForm(this.form, this.object, this.group, this.fieldControll);
	};
	
	
	function generateForm(form, obj, group, fieldControll) {
		var result = '';
		for (var propName in form) {
			if(group && group != form[propName].group) {
				// if group defined and field is not in it
				v("Group:"+group);
				continue;
			}
			var label = form[propName].label || "Label";
			var controll = "";			  	
			if(form[propName].type == "multiselect" ) {
				controll = '<select multiple="multiple" id="'+propName+'">';
				var options = form[propName].options();
				for(o in options) {				
					var selected = inArray(options[o].value, obj[propName]) ? "selected" : "";
					result += '<option value="'+options[o].value+'" '+selected+' >'+options[o].title+'</option>';
				};
				controll += '</select>';			
			} else if(form[propName].type == "selectYear" ) {
				controll = '<select id="'+propName+'">';
				var options = form[propName].options();
				var year = "";
				if(obj[propName]) {
					year = new Date(obj[propName].split(" ")[0]).getFullYear();
				} else {
					controll += '<option value="" selected ></option>';
				}
				for(o in options){
					var selected = options[o].value == year ? "selected" : "";
					controll += '<option value="'+options[o].value+'" '+selected+' >'+options[o].title+'</option>';
				};
				controll += '</select>';			
			} else if(form[propName].type == "htmledit" ) {
				controll = '<textarea class="htmledit" id="'+propName+'">'
					+(obj[propName] || "")
					+'</textarea>';
			} else {
				var generator = forms.getGenerators()[form[propName].type];
				if(generator) {
					controll = generator.generateField(form, obj, propName);
				}
			}
			v("Control:"+label+" "+controll);
			result += Template[fieldControll || 'fieldControllGroup']({controll:controll,label:label});					
			//v("Control:"+label+" done");
		}	
		return result;
	}
	
	function inArray(obj, array) {
		for(i in array) {
			if(obj == array[i]) {
				return true;
			}
		}
		return false;
	}
		
	Handlebars.registerHelper('formParamsDeprecated',function(formName, obj, fieldControll){
	    var obj = Session.get(obj) || {};
	    // some helper arguments hack
		var form = forms.getForm(formName);
	    if(fieldControll.hash) {
			return {formName:formName, form:form, object: obj};
		} else {
			return {formName:formName, form:form, object: obj, fieldControll: fieldControll};
		}
	});

	
};	
	

	
	